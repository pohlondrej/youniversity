package com.youniversity.core

class LogFilters {

    def filters = {
        all(controller:'*', action:'*') {
            before = {
                log.info(!params.controller ? '/: ' + params : params.controller +"."+(params.action ?: "index")+": "+params)
            }
        }
    }
}
