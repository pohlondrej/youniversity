import com.youniversity.core.user.Role
import com.youniversity.core.user.Group
import com.youniversity.core.user.GroupRole
import com.youniversity.core.user.Permission
import com.youniversity.core.app.Application
import com.youniversity.core.user.User
import com.youniversity.core.user.UserRole
import com.youniversity.apps.achievement.UserAchievementCriteria

class BootStrap {

    def grailsApplication
    def springSecurityService

    def init = { servletContext ->
        Role memberRole = new Role(authority:"MEMBER").save()
        Role adminRole = new Role(authority:"ADMIN").save()
        Role animalRole = new Role(authority:"MASTER").save()
        Group userGroup = new Group(name:"USER").save()
        Group ctuGroup = new Group(name:"CTU").save()
        Group animalGroup = new Group(name:"ANIMAL").save()
        GroupRole userMember = new GroupRole(group:userGroup, role:memberRole).save()
        GroupRole ctuMember = new GroupRole(group:ctuGroup, role:memberRole).save()
        GroupRole userAdmin = new GroupRole(group:userGroup, role:adminRole).save()
        GroupRole ctuAdmin = new GroupRole(group:ctuGroup, role:adminRole).save()
        GroupRole animalUser = new GroupRole(group: animalGroup, role:memberRole).save()

        Application app = new Application(name:"Untitled1",displayedName: "todo",runPath: "todo/index",widgetable: false).save()
        new Application(name:"Untitled2",displayedName: "sdfsdf",runPath: "user/list",widgetable: false).save()
        new Application(name:"Untitled3",displayedName: "dsdsg",runPath: "user/create",widgetable: false).save()
        new Application(name:"Untitled4",displayedName: "sdggsdo",runPath: "user/show",widgetable: false).save()
        new Application(name:"Untitled5",displayedName: "sdggsdo",runPath: "user/show", widgetable: true, widgetRunPath: "todo/renderWidget").save()
        new Application(name:"Untitled6",displayedName: "sdggsdo",runPath: "user/show", widgetable: true, widgetRunPath: "todo/renderWidget").save()
        new Application(name:"Untitled7",displayedName: "sdggsdo",runPath: "user/show", widgetable: true, widgetRunPath: "todo/renderWidget").save()
        new Application(name:"Untitled8",displayedName: "sdggsdo",runPath: "user/show", widgetable: true, widgetRunPath: "todo/renderWidget").save()

        User user = new User(username: "user", password:springSecurityService.encodePassword("user"),email: "user@youniversity.cz", enabled: true ).save()
        User admin = new User(username: "admin", password:springSecurityService.encodePassword("admin"),email: "admin@youniversity.cz", enabled: true ).save()

        new UserRole(user: user,groupRole: userMember).save()
        new UserRole(user:admin, groupRole: userAdmin).save()
        new UserAchievementCriteria(user:user.id,done: true).save()
        grailsApplication.controllerClasses.each { controller ->
            controller.uris.each { String uri ->
                def configAttribute="ROLE_USER_ADMIN"
                def roles= controller?.getPropertyValue("permissions")?.getAt(uri)?.getAt("configAttribute")
                if(roles)
                    if(roles.contains("IS_AUTHENTICATED_ANONYMOUSLY"))
                        configAttribute="IS_AUTHENTICATED_ANONYMOUSLY"
                    else if(roles.contains("ROLE_USER_ADMIN"))
                        configAttribute=roles
                    else
                        configAttribute+=",${roles}"
                if(!uri.contains("register") && !uri.contains("auth") && !uri.contains("login") && !uri.contains("logout") && !uri.contains("get-all-apps"))
                    new Permission(url:"${uri}",configAttribute:configAttribute,application:app, authMethod:controller?.getPropertyValue("permissions")?.getAt(uri)?.getAt("authMethod")).save()
                else
                    new Permission(url:"${uri}",configAttribute: "IS_AUTHENTICATED_ANONYMOUSLY",application:app).save()
            }
        }

    }
    def destroy = {
    }
}
