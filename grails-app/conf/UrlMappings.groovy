class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?"{
            constraints {
                // apply constraints here
            }
        }
        "/app/$appName"(controller:'application', action:'run')
        "/widget/$appName"(controller:'application', action:'runWidget')


        "/"(view:"/index")
		"500"(view:'/error')
	}
}
