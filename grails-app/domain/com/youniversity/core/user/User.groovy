package com.youniversity.core.user

import com.youniversity.core.app.FavouriteApp

class User {

	transient springSecurityService

	String username
	String password
    String email
    String firstName
    String lastName
    String university
    String faculty
    String shortInfo

	boolean enabled
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired
    String picturePath

    static hasMany = [favouriteApps:FavouriteApp]
        
	static constraints = {
		username blank: false, unique: true
		password blank: false
        picturePath nullable: true
        firstName nullable: true, blank: true
        lastName nullable: true, blank: true
        university nullable: true, blank: true
        faculty nullable: true, blank: true
        shortInfo nullable: true, blank: true
	}

	static mapping = {
		password column: '`password`'
	}

	Set<GroupRole> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.groupRole } as Set
	}

	def beforeInsert() {

	}

	def beforeUpdate() {

	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}
}
