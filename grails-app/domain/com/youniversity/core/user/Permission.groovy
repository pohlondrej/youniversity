package com.youniversity.core.user

import com.youniversity.core.app.Application
class Permission {
    String url
    String configAttribute
    Application application
    Map<String, Closure> authMethod

    static mapping = {
        cache true
    }

    static constraints = {
        url blank: false, unique: true
        configAttribute blank: false
        authMethod nullable: true
    }
}
