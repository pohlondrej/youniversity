package com.youniversity.core.user

import org.apache.commons.lang.builder.HashCodeBuilder

class UserRole implements Serializable {

	User user
	GroupRole groupRole

	boolean equals(other) {
		if (!(other instanceof UserRole)) {
			return false
		}

		other.user?.id == user?.id &&
			other.groupRole?.id == groupRole?.id
	}

	int hashCode() {
		def builder = new HashCodeBuilder()
		if (user) builder.append(user.id)
		if (groupRole) builder.append(groupRole.id)
		builder.toHashCode()
	}

	static UserRole get(long userId, long groupRoleId) {
		find 'from UserRole where user.id=:userId and groupRole.id=:groupRoleId',
			[userId: userId, groupRoleId: groupRoleId]
	}

	static UserRole create(User user, GroupRole groupRole, boolean flush = false) {
		new UserRole(user: user, groupRole: groupRole).save(flush: flush, insert: true)
	}

	static boolean remove(User user, GroupRole groupRole, boolean flush = false) {
		UserRole instance = UserRole.findByUserAndGroupRoleRole(user, groupRole)
		if (!instance) {
			return false
		}

		instance.delete(flush: flush)
		true
	}

	static void removeAll(User user) {
		executeUpdate 'DELETE FROM UserRole WHERE user=:user', [user: user]
	}

	static void removeAll(Role groupRole) {
		executeUpdate 'DELETE FROM UserRole WHERE groupRole=:groupRole', [groupRole: groupRole]
	}

	static mapping = {
		version false
	}
}
