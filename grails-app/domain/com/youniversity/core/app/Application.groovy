package com.youniversity.core.app

class Application {
    String name
    String displayedName
    String runPath
    boolean widgetable
    String widgetRunPath

    static constraints = {
        name nullable:false, blank:false, unique:true
        runPath nullable:false, blank:false
        displayedName nullable:true
        widgetRunPath nullable:true, validator: { val, obj ->
            !obj.widgetable || obj.widgetRunPath
        }
    }
}
