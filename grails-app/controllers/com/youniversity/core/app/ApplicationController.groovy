package com.youniversity.core.app



import grails.transaction.*
import static org.springframework.http.HttpStatus.*
import grails.converters.JSON
import com.youniversity.core.AuthenticationController

@Transactional(readOnly = true)
class ApplicationController extends AuthenticationController {

    def springSecurityService
    def authenticationService

    def permissions = [
            "/application/index":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod": [
                            "ROLE_USER_MEMBER":"truth"
                    ]
            ],
            "/application/run/**":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod": [
                            "ROLE_USER_MEMBER":"truth"
                    ]
            ],
            "/application/runWidget/**":["configAttribute":"ROLE_USER_MEMBER"],
            "/application/get-all-apps":["configAttribute":"IS_AUTHENTICATED_ANONYMOUSLY"]
    ]


    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Application.list(params), model:[applicationInstanceCount: Application.count()]
    }

    def show(Application applicationInstance) {
        respond applicationInstance
    }

    def create() {        
        respond new Application(params)
    }

    @Transactional
    def save(Application applicationInstance) {
        if(applicationInstance.hasErrors()) {
            respond applicationInstance.errors, view:'create'
        }
        else {
            applicationInstance.save flush:true
            request.withFormat {
                form {
                    springSecurityService.clearCachedRequestmaps()
                    flash.message = message(code: 'default.created.message', args: [message(code: 'applicationInstance.label', default: 'Application'), applicationInstance.id])
                    redirect applicationInstance
                }
                '*' { respond applicationInstance, [status: CREATED] }
            }
        }
    }

    def run(String appName) {
        def app=Application.findByName(appName)
        if(!app || !app.runPath)        {
            render(view:"appNotFound",model:[appName:appName])
            return
        }
        redirect(url: "/${app?.runPath}")
    }

    def runWidget(String appName) {
        def app=Application.findByName(appName)
        if(!app || !app.widgetable || !app.widgetRunPath)        {
            render text: g.message([code:"youniversity.core.widget.notFound",args:[appName]])
            return
        }
        redirect(url: "/${app?.widgetRunPath}")
    }

    def getAllApps()    {
        def apps = Application.list()
        def response = []

        apps.each{
            response << ["displayedName":it.displayedName,"name":it.name, "widgetable": it.widgetable]
        }

        render response as JSON
    }

    def edit(Application applicationInstance) { 
        respond applicationInstance  
    }

    @Transactional
    def update(Application applicationInstance) {
        if(applicationInstance == null) {
            render status:404
        }
        else if(applicationInstance.hasErrors()) {
            respond applicationInstance.errors, view:'edit'
        }
        else {
            applicationInstance.save flush:true
            request.withFormat {
                form {
                    springSecurityService.clearCachedRequestmaps()
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'Application.label', default: 'Application'), applicationInstance.id])
                    redirect applicationInstance 
                }
                '*'{ respond applicationInstance, [status: OK] }
            }
        }        
    }

    @Transactional
    def delete(Application applicationInstance) {
        if(applicationInstance) {
            applicationInstance.delete flush:true
            request.withFormat {
                form { 
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'Application.label', default: 'Application'), applicationInstance.id])
                    redirect action:"index", method:"GET" 
                }
                '*'{ render status: NO_CONTENT }
            }                
        }
        else {
            render status: NOT_FOUND
        }
    }
}

