package com.youniversity.core.user

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest

class UserController {

    def mediaService

    def permissions = [
            "/user/show/**":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod":[
                            "ROLE_USER_MEMBER":"true"
                    ]
            ],
            "/user/list":["configAttribute":"ROLE_USER_MEMBER"]
    ]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [userInstanceList: User.list(params), userInstanceTotal: User.count()]
    }

    def create() {
        [userInstance: new User(params)]
    }

    def save() {
        def userInstance = new User(params)
        if (!userInstance.save(flush: true)) {
            render(view: "create", model: [userInstance: userInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), userInstance.id])
        redirect(action: "show", id: userInstance.id)
    }

    def show(Long id) {
        def userInstance = User.get(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }

        [userInstance: userInstance]
    }

    def edit(Long id) {
        def userInstance = User.get(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }


        [userInstance: userInstance]
    }

    def update(Long id, Long version) {
        def userInstance = User.get(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (userInstance.version > version) {
                userInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'user.label', default: 'User')] as Object[],
                          "Another user has updated this User while you were editing")
                render(view: "edit", model: [userInstance: userInstance])
                return
            }
        }


        userInstance.properties = params
        def root = grailsApplication.config.youniversity.upload.home

        MultipartFile profile = request.getFile("profilePicture")
        def checkFileExtension = mediaService.checkFileExtension(profile, "jpg png gif jpeg")
        if (checkFileExtension.result==false) {
            userInstance.errors.rejectValue("version", "something",
                    [message(code: 'user.wrongFileExtension.label', default: 'User')] as Object[],
                    "File extension is wrong")
            render(view: "edit", model: [userInstance: userInstance])
            return
        }

        if (mediaService.checkImageDimensions(profile, 120, 80, false).result==false) {
            userInstance.errors.rejectValue("version", "something",
                    [message(code: 'user.wrongFileExtension.label', default: 'User')] as Object[],
                    "File size is wrong")
            render(view: "edit", model: [userInstance: userInstance])
            return
        }


        def user = User.findById(userInstance.id)
        user.picturePath = root + File.separator+"profile"+ File.separator + userInstance.id+"."+checkFileExtension.extension
        user.save(flush: true)

        mediaService.uploadFile(profile,userInstance.id+"."+checkFileExtension.extension,root + File.separator+"profile")


        if (!userInstance.save(flush: true)) {
            render(view: "edit", model: [userInstance: userInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), userInstance.id])
        redirect(action: "show", id: userInstance.id)
    }

    def getPictureFromDisk(Long id){
        def user = User.findById(id)
        mediaService.getPictureFromDisk(user.picturePath,true)
    }

    def delete(Long id) {
        def userInstance = User.get(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }

        try {
            userInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "show", id: id)
        }
    }
}
