package com.youniversity.core.user

import org.springframework.dao.DataIntegrityViolationException

class GroupRoleController {

    def permissions =[
    ]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [groupRoleInstanceList: GroupRole.list(params), groupRoleInstanceTotal: GroupRole.count()]
    }

    def create() {
        [groupRoleInstance: new GroupRole(params)]
    }

    def save() {
        def groupRoleInstance = new GroupRole(params)
        if (!groupRoleInstance.save(flush: true)) {
            render(view: "create", model: [groupRoleInstance: groupRoleInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'groupRole.label', default: 'GroupRole'), groupRoleInstance.id])
        redirect(action: "show", id: groupRoleInstance.id)
    }

    def show(Long id) {
        def groupRoleInstance = GroupRole.get(id)
        if (!groupRoleInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'groupRole.label', default: 'GroupRole'), id])
            redirect(action: "list")
            return
        }

        [groupRoleInstance: groupRoleInstance]
    }

    def edit(Long id) {
        def groupRoleInstance = GroupRole.get(id)
        if (!groupRoleInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'groupRole.label', default: 'GroupRole'), id])
            redirect(action: "list")
            return
        }

        [groupRoleInstance: groupRoleInstance]
    }

    def update(Long id, Long version) {
        def groupRoleInstance = GroupRole.get(id)
        if (!groupRoleInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'groupRole.label', default: 'GroupRole'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (groupRoleInstance.version > version) {
                groupRoleInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'groupRole.label', default: 'GroupRole')] as Object[],
                          "Another user has updated this GroupRole while you were editing")
                render(view: "edit", model: [groupRoleInstance: groupRoleInstance])
                return
            }
        }

        groupRoleInstance.properties = params

        if (!groupRoleInstance.save(flush: true)) {
            render(view: "edit", model: [groupRoleInstance: groupRoleInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'groupRole.label', default: 'GroupRole'), groupRoleInstance.id])
        redirect(action: "show", id: groupRoleInstance.id)
    }

    def delete(Long id) {
        def groupRoleInstance = GroupRole.get(id)

        if (!groupRoleInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'groupRole.label', default: 'GroupRole'), id])
            redirect(action: "list")
            return
        }

        try {

            groupRoleInstance.delete(flush: true)

            flash.message = message(code: 'default.deleted.message', args: [message(code: 'groupRole.label', default: 'GroupRole'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'groupRole.label', default: 'GroupRole'), id])
            redirect(action: "show", id: id)
        }
    }
}
