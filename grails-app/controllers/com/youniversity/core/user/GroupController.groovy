package com.youniversity.core.user

import grails.converters.JSON

//import org.codehaus.plexus.util.CollectionUtils
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.multipart.MultipartFile

class GroupController {
    def mediaService
    def springSecurityService

    def permissions =[
            "/group/index":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod":[
                            "ROLE_USER_MEMBER":"true"
                    ]
            ],
            "/group/create":["configAttribute":"ROLE_USER_MEMBER"],
            "/group/save/**":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod":[
                            "ROLE_USER_MEMBER":"true"
                    ]
            ],
            "/group/save":["configAttribute":"ROLE_USER_MEMBER"],
            "/group/edit":["configAttribute":"ROLE_USER_MEMBER"],
            "/group/update/**":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod":[
                            "ROLE_USER_MEMBER":"true"
                    ]
            ],
            "/group/delete/**":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod":[
                            "ROLE_USER_MEMBER":"true"
                    ]
            ],
            "/group/show/**":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod":[
                            "ROLE_USER_MEMBER":"true"
                    ]
            ],
            "/group/list":["configAttribute":"ROLE_USER_MEMBER"],
            "/group/join/**":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod":[
                            "ROLE_USER_MEMBER":"true"
                    ]
            ],
            "/group/leave/**":["configAttribute":"ROLE_USER_MEMBER",
                    "authMethod":[
                            "ROLE_USER_MEMBER":"true"
                    ]
            ]
    ]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [groupInstanceList: Group.list(params), groupInstanceTotal: Group.count()]
    }

    def create() {
        [groupInstance: new Group(params)]
    }

    def save() {
        Group groupInstance = new Group(params)


        if (!groupInstance.save(flush: true)) {
            render(view: "create", model: [groupInstance: groupInstance])
            return
        }


        new GroupRole(group: groupInstance,role: Role.findByAuthority("MEMBER")).save()
        def roleAdmin = new GroupRole(group: groupInstance,role: Role.findByAuthority("ADMIN")).save()

        new UserRole(user:springSecurityService.getCurrentUser(), groupRole:roleAdmin).save()



        flash.message = message(code: 'default.created.message', args: [message(code: 'group.label', default: 'Group'), groupInstance.id])
        redirect(action: "show", id: groupInstance.id)
    }

    def show(Long id) {
        def groupInstance = Group.get(id)
        if (!groupInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'group.label', default: 'Group'), id])
            redirect(action: "list")
            return
        }

        def users=[:]
        groupInstance.groupRoles.each{
                 users.put(it.id,UserRole.findAllByGroupRole(it).collect{it.user})
        }


        [groupInstance: groupInstance, users:users]
    }

    def edit(Long id) {
        def groupInstance = Group.get(id)
        if (!groupInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'group.label', default: 'Group'), id])
            redirect(action: "list")
            return
        }
        def users=[:]
        groupInstance.groupRoles.each{
            users.put(it.id,UserRole.findAllByGroupRole(it).collect{it.user})
        }


        [groupInstance: groupInstance, users:users]


    }


    def update(Long id, Long version) {
        def groupInstance = Group.get(id)
        if (!groupInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'group.label', default: 'Group'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (groupInstance.version > version) {
                groupInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'group.label', default: 'Group')] as Object[],
                          "Another user has updated this Group while you were editing")
                render(view: "edit", model: [groupInstance: groupInstance])
                return
            }
        }

        groupInstance.properties = params



        def root = grailsApplication.config.youniversity.upload.home

        MultipartFile profile = request.getFile("groupProfilePicture")
        def checkFileExtension = mediaService.checkFileExtension(profile, "jpg png gif jpeg")


        if (!checkFileExtension.result) {
            groupInstance.errors.rejectValue("version", "something",
                    [message(code: 'user.wrongFileExtension.label', default: 'Group')] as Object[],
                    "File extension is wrong")
            render(view: "edit", model: [groupInstance: userInstance])
            return
        }


        if (!mediaService.checkImageDimensions(profile, 120, 80, false).result) {
            groupInstance.errors.rejectValue("version", "something",
                    [message(code: 'user.wrongFileExtension.label', default: 'Group')] as Object[],
                    "File size is wrong")
            render(view: "edit", model: [groupInstance: groupInstance])
            return
        }

        def group = Group.findById(groupInstance.id)
        group.picturePath = root + File.separator+"groupProfile"+ File.separator + groupInstance.id+"."+checkFileExtension.extension
        group.save(flush: true)

        mediaService.uploadFile(profile,groupInstance.id+"."+checkFileExtension.extension,root + File.separator+"groupProfile")

        if (!groupInstance.save(flush: true)) {
            render(view: "edit", model: [groupInstance: groupInstance])
            return
        }

        redirect(action: "show", id: groupInstance.id)
    }

    def getPictureFromDisk(Long id){
        def group = Group.findById(id)
        mediaService.getPictureFromDisk(group.picturePath,true)
    }

    def delete(Long id) {
        def groupInstance = Group.get(id)
        if (!groupInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'group.label', default: 'Group'), id])
            redirect(action: "list")
            return
        }

        try {
            groupInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'group.label', default: 'Group'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'group.label', default: 'Group'), id])
            redirect(action: "show", id: id)
        }


    }

    def addRole()   {
        if(GroupRole.findByGroupAndRole(Group.get(params.id),Role.findByAuthority(params.authority))) {
            flash.message = message(code: 'youniversity.core.group.role.exist',default:"Role ${params.authority} already exists")
            redirect (action: "edit", id: params.id)
            return
        }
        new GroupRole(role:Role.findByAuthority(params.authority)?:new Role(authority: params.authority).save(),
                      group:Group.get(params.id)).save()

        redirect action: "edit", id: params.id

    }

    def removeRole(Long id)    {
        def groupRoleInstance = GroupRole.get(id)

        if (!groupRoleInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'groupRole.label', default: 'GroupRole'), id])
            redirect(action: "edit", id:groupRoleInstance.group.id)
            return
        }

        try {

            groupRoleInstance.delete(flush: true)

            flash.message = message(code: 'default.deleted.message', args: [message(code: 'groupRole.label', default: 'GroupRole'), id])
            redirect(action: "edit", id:groupRoleInstance.group.id)
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'groupRole.label', default: 'GroupRole'), id])
            redirect(action: "edit", id:groupRoleInstance.group.id)
        }
    }

    def manageUsers(Long id, Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [groupRoleId: id, userInstanceList: UserRole.findAllByGroupRole(GroupRole.get(id),params)?.user?.unique(), userInstanceTotal: UserRole.findAllByGroupRole(GroupRole.get(id))?.user.unique().size()]
    }

    def managePermissions(Long id, Integer max)  {
        permissions = Permission.findAllByConfigAttributeLike("%${GroupRole.get(id).authority}%",params)
        params.max = Math.min(max ?: 10, 100)
        [groupRoleId: id, permissionInstanceList:permissions,permissionInstanceTotal:Permission.findAllByConfigAttributeLike("%${GroupRole.get(id).authority}%").size()]
    }

    def addUser()   {
        def groupRoleInstance = GroupRole.get(params.id)
        if(!User.findByUsername(params.username))          {
            flash.message = message(code: 'youniversity.core.group.user.notExist',default:"User ${params.username} doesn't exists")
            redirect (action: "manage-users", id: params.id)
            return
        }

        if(UserRole.findByUserAndGroupRole(User.findByUsername(params.username), groupRoleInstance))   {
            flash.message = message(code: 'youniversity.core.group.user.exist',default:"User ${params.username} is already ${groupRoleInstance.role.authority}")
            redirect (action: "manage-users", id: params.id)
            return
        }
        new UserRole(user:User.findByUsername(params.username), groupRole: groupRoleInstance).save()
        redirect action:"manage-users", id:groupRoleInstance.group.id
    }

    def removeUser() {
        def groupRoleInstance = GroupRole.get(params.groupRoleId)
        println groupRoleInstance
        if(!User.get(params.id))          {
            flash.message = message(code: 'youniversity.core.group.user.notExist',default:"User doesn't exists")
            redirect (action: "manage-users", id: groupRoleInstance.group.id)
            return
        }
        if(!UserRole.findByUserAndGroupRole(User.get(params.id), groupRoleInstance))   {
            flash.message = message(code: 'youniversity.core.group.user.notMember',default:"User ${User.get(params.id).username} is not ${groupRoleInstance.role.authority}")
            redirect (action: "manage-users", id: groupRoleInstance.group.id)
            return
        }
        if(GroupRole.findByGroupAndRole(groupRoleInstance.group, Role.findByAuthority("ADMIN"))){
            flash.message = message(code: 'youniversity.core.group.user.notPermited',default:"User ${User.get(params.id).username} can't be deleted")
            redirect (action: "manage-users", id: groupRoleInstance.group.id)
            return
        }
        def userRoleInstance = UserRole.findByGroupRoleAndUser(groupRoleInstance, User.get(id))
        userRoleInstance.delete()
        redirect action:"manage-users", id:groupRoleInstance.group.id
    }

    def removePermission()  {
        def permission = Permission.get(params.id)
        permission.configAttribute=permission.configAttribute.replaceAll(GroupRole.get(params.groupRoleId).authority,"")
        permission.save()
        println permission.configAttribute
        springSecurityService.clearCachedRequestmaps()
        redirect action:"manage-permissions", id:params.groupRoleId
        return
    }

    def addPermission() {

    }

    def join(Long id)   {
        def groupRoleInstance = GroupRole.findByGroupAndRole(Group.get(id), Role.findByAuthority("MEMBER"))
        new UserRole(user:springSecurityService.getCurrentUser(),groupRole: groupRoleInstance).save()
        redirect action:"list"
    }

    def leave(Long id)
    {
        def groupRolesInstance = GroupRole.findAllByGroup(Group.get(id))
        UserRole.findAllByUserAndGroupRoleInList(springSecurityService.getCurrentUser(),groupRolesInstance).each {it.delete()}
        redirect action:"list"
    }

    def getAllUsers()    {


        def users = User.list()
        def response = []
        users.each{
            response << it.username
        }

        render response as JSON
    }
}
