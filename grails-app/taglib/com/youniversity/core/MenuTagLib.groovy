package com.youniversity.core

import org.codehaus.groovy.grails.web.pages.discovery.GrailsConventionGroovyPageLocator
class MenuTagLib {
    GrailsConventionGroovyPageLocator groovyPageLocator
    //static encodeAsForTags = [tagName: 'raw']
    static namespace = "g"
    def renderIfExists = { attrs,body->
        if(groovyPageLocator.findTemplate(attrs.template))  {
            if(!attrs.plugin)   {
                out << g.render(template:attrs.template)
            }
            else {
                out << g.render(template:attrs.template, plugin: attrs.plugin)
            }
        }
        else    {
            if(attrs.plugin)   {
                try {
                    out << g.render(template:"/layouts/sidebar", plugin: attrs.plugin)
                }
                catch (GroovyPagesException){
                    out << g.render(template: "/layouts/sidebar")
                }
            }
            else    {
                out << g.render(template: "/layouts/sidebar")
            }
        }
    }
}
