package com.youniversity.core

import grails.transaction.Transactional

@Transactional
/**
 * This service is used for injecting classes between plugins and main applications
 * it uses syntax NameOfTheClassFromNameOfThePackage to specify dynamically which class
 * should be injected
 * @author Richard Holaj
 * @version 0.1
 */
class ClassService {
    /**
     * Contains applicationContext to obtain classes
     */
    def grailsApplication

    /**
     * This method is called every time when called method from Service does not exist,
     * in this case everytime. Method splits called name by From, if "From" isn't in called name, method logs error. Before "From" is className,
     * which is already in right format. After "From" is a prefix ~ namespace.
     * Prefix has to be transfered to right format first, so from NameOfTheNamespace to .name.of.the.namespace
     * If prefox does not contains core, we add .apps to its beggining.
     * Full name of class is constructed by joining com.youniversity with prefix and className after .
     * If class does not exist, error is logged
     * @param name name of the method
     * @param args arguments of the method (unused)
     * @return class to be injected or logs error
     */
    def methodMissing(String name, args) {
        if(name ==~ /.*From.*/) {
            def parts = name.split("From",2)
            def prefix = parts[1]
            def className = parts[0]
            prefix = prefix.replaceAll('[A-Z]'){String it -> "."+it.toLowerCase()}
            if(!prefix.contains("core"))
                prefix= ".apps"+prefix
            return grailsApplication.getClassForName("com.youniversity${prefix}.${className}")?:log.error("Class ${className} in namespace com.youniversity${prefix} does not exist!")
        }
        else
            log.error("Wrong classFinder syntax ${name}")
    }
}
