package com.youniversity.core

import grails.transaction.Transactional
import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import org.springframework.web.multipart.MultipartFile
import java.awt.image.RenderedImage
import org.codehaus.groovy.grails.web.util.WebUtils

@Transactional
class MediaService {

    boolean transactional = true

    String getFileExtension(fileName) {
        int i = fileName.lastIndexOf('.')
        if (i > 0) {
            return fileName.substring(i + 1).toLowerCase()
        }
        return ""
    }

    def checkFileExtension(file, extensions) {
        def fileName = file.originalFilename.toString()
        String extension = getFileExtension(fileName)

        if (extensions.contains(extension)) {
            return [result: true, extension: extension]
        } else {
            return [result: false]
        }
    }

    def checkImageDimensions(imageFile, requiredWidth, requiredHeight, boolean exact) {

        BufferedImage image = ImageIO.read(imageFile.inputStream)
        def imageHeight = image.getHeight()
        def imageWidth = image.getWidth()

        if (imageWidth <= requiredWidth && imageHeight <= requiredHeight)
            if(!exact || (exact && imageWidth == requiredWidth && imageHeight == requiredHeight))
                return [result: true, imageWidth: imageWidth, imageHeight: imageHeight]
        return [result: false]
    }


    static def checkDirectory(def directoryPath) {

        File origFile = new File(directoryPath)
        if (!origFile.exists()) {
            new File(directoryPath).mkdirs()
        }
    }

    def String uploadFile(MultipartFile file, String name, String destinationDirectory) {
        def storagePath = destinationDirectory

        def storagePathDirectory = new File(storagePath)
        if (!storagePathDirectory.exists()) {
            storagePathDirectory.mkdirs()
        }

        if (!file.isEmpty()) {
            file.transferTo(new File("${storagePath}/${name}"))
            return "${storagePath}/${name}"
        } else {
            return null
        }
    }

    def getPictureFromDisk (String path, boolean isImage) {
        if (path != null) {
            File file = new File(path)
            def webUtils = WebUtils.retrieveGrailsWebRequest()
            def response = webUtils.getCurrentResponse()
            if(isImage)      {
                RenderedImage originalImage = ImageIO.read(file)
                OutputStream byteArrayOutputStream = new ByteArrayOutputStream()
                String extension = getFileExtension(file.absolutePath)
                ImageIO.write(originalImage, extension, byteArrayOutputStream)
                byte[] imageInByte = byteArrayOutputStream.toByteArray()
                response.setHeader('Content-length', imageInByte.length.toString())
                response.contentType = 'image/' + extension
                response.outputStream << imageInByte
            }   else    {
                response.setContentType("application/octet-stream")
                response.setHeader("Content-disposition", "attachment;filename=${file.getName()}")
                response.outputStream << file.newInputStream()
                response.outputStream.flush()
            }
            return response
        }
    }
}
