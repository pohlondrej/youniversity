<%@ page import="com.youniversity.core.app.Application" %>



<div class="fieldcontain ${hasErrors(bean: applicationInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="application.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${applicationInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: applicationInstance, field: 'runPath', 'error')} required">
	<label for="runPath">
		<g:message code="application.runPath.label" default="Run Path" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="runPath" required="" value="${applicationInstance?.runPath}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: applicationInstance, field: 'displayedName', 'error')} ">
	<label for="displayedName">
		<g:message code="application.displayedName.label" default="Displayed Name" />
		
	</label>
	<g:textField name="displayedName" value="${applicationInstance?.displayedName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: applicationInstance, field: 'widgetable', 'error')} ">
	<label for="widgetable">
		<g:message code="application.widgetable.label" default="Widgetable" />
		
	</label>
	<g:checkBox name="widgetable" value="${applicationInstance?.widgetable}" />
</div>

<div class="fieldcontain ${hasErrors(bean: applicationInstance, field: 'widgetRunPath', 'error')} ">
	<label for="widgetRunPath">
		<g:message code="application.widgetRunPath.label" default="Widget Run Path" />
		
	</label>
	<g:textField name="widgetRunPath" value="${applicationInstance?.widgetRunPath}"/>
</div>

