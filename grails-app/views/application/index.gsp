
<%@ page import="com.youniversity.core.app.FavouriteApp; com.youniversity.core.app.Application" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'application.label', default: 'Application')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-application" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-application" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>

						<g:sortableColumn property="name" title="${message(code: 'application.name.label', default: 'Name')}" />

						<g:sortableColumn property="runPath" title="${message(code: 'application.runPath.label', default: 'Run Path')}" />

						<g:sortableColumn property="displayedName" title="${message(code: 'application.displayedName.label', default: 'Displayed Name')}" />

						<g:sortableColumn property="widgetable" title="${message(code: 'application.widgetable.label', default: 'Widgetable')}" />

						<g:sortableColumn property="widgetRunPath" title="${message(code: 'application.widgetRunPath.label', default: 'Widget Run Path')}" />

					</tr>
				</thead>
				<tbody>
				<g:each in="${applicationInstanceList}" status="i" var="applicationInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

						<td><g:link action="show" id="${applicationInstance.id}">${fieldValue(bean: applicationInstance, field: "name")}</g:link></td>

						<td>${fieldValue(bean: applicationInstance, field: "runPath")}</td>

						<td>${fieldValue(bean: applicationInstance, field: "displayedName")}</td>

						<td><g:formatBoolean boolean="${applicationInstance.widgetable}" /></td>

						<td>${fieldValue(bean: applicationInstance, field: "widgetRunPath")}</td>

                        <g:if test="${FavouriteApp.findByApplicationAndWidget(applicationInstance,false)}" >
                            <td><g:link id="${applicationInstance.id}" action="remove" controller="favouriteApp">Remove</g:link></td>
                        </g:if>
                        <g:else>
                            <td>
                                 <g:link id="${applicationInstance.id}" action="add" controller="favouriteApp">Add</g:link>
                            </td>
                        </g:else>
                        <g:if test="${applicationInstance.widgetable}">
                        <g:if test="${FavouriteApp.findByApplicationAndWidget(applicationInstance,true)}" >
                            <td><g:link id="${applicationInstance.id}" action="removeWidget" controller="favouriteApp">Remove</g:link></td>
                        </g:if>
                        <g:else>
                            <td>
                                <g:link id="${applicationInstance.id}" action="addWidget" controller="favouriteApp">Add</g:link>
                            </td>
                        </g:else>
                        </g:if>
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${applicationInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
