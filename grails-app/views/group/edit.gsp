<%@ page import="com.youniversity.core.user.GroupRole; com.youniversity.core.user.Group; com.youniversity.core.user.User" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'group.label', default: 'Group')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>

		<div id="edit-group" class="content scaffold-edit" role="main">
			<h1><g:message code="default.edit.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${groupInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${groupInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form method="post" enctype="multipart/form-data" >
				<g:hiddenField name="id" value="${groupInstance?.id}" />
				<g:hiddenField name="version" value="${groupInstance?.version}" />
				<fieldset class="form">
					<g:render template="formEdit"/>
				</fieldset>


				<fieldset class="buttons">
					<g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
            <ul>
                <g:each in="${groupInstance?.groupRoles?}" var="g">
                    <li>${g.role.authority}
                    <g:link controller="group" action="manageUsers" id="${g.id}">Manage users</g:link> <g:link controller="group" action="managePermissions" id="${g.id}">Manage Permissions</g:link>
                    <g:if test="${g?.role?.authority!='ADMIN' && g?.role?.authority!='MEMBER'}"> <g:link controller="group" action="removeRole" id="${g.id}"> Remove</g:link></g:if>
                    </li>
                </g:each>
                <li>
                    <g:form method="post" >
                        <g:hiddenField name="id" value="${groupInstance?.id}" />
                        <g:hiddenField name="version" value="${groupInstance?.version}" />
                        <fieldset class="form">
                            <g:textField name="authority" id="role-search"></g:textField>
                        </fieldset>


                        <fieldset class="buttons">
                            <g:actionSubmit class="save" action="addRole" value="${message(code: 'default.button.add.label', default: 'Add')}" />
                        </fieldset>
                    </g:form>
                </li>
            </ul>



        </div>
    <script>
        $(function() {
            $( "#role-search" ).autocomplete({
                source: "/YOUniversity/role/get-all-roles/${groupInstance.id}"
            });
        });
    </script>
	</body>
</html>
