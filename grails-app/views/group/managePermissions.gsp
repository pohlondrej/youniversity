
<%@ page import="com.youniversity.core.user.GroupRole; com.youniversity.core.user.User;com.youniversity.core.user.UserRole" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
		<title><g:message code="default.list.label" args="["Permission"]" /></title>
	</head>
	<body>
		<div id="list-user" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="["Permission"]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <ul>
                <g:each in="${permissionInstanceList}" status="i" var="permissionInstance">
                    <li>${fieldValue(bean: permissionInstance, field: "url")}
                        <g:form method="post" >
                            <g:hiddenField name="groupRoleId" value="${groupRoleId}" />
                            <g:hiddenField name="id" value="${permissionInstance.id}" />
                            <fieldset class="buttons">
                                <g:actionSubmit class="save" action="removePermission" value="${message(code: 'default.button.remove.label', default: 'Remove')}" />
                            </fieldset>
                        </g:form>

                    </li>
                </g:each>
                <g:form method="post" >
                    <g:hiddenField name="id" value="${groupRoleId}" />
                    <fieldset class="form">
                        <g:textField name="permission" id="permission-search"></g:textField>
                    </fieldset>
                    <fieldset class="buttons">
                        <g:actionSubmit class="save" action="addPermission" value="${message(code: 'default.button.add.label', default: 'Add')}" />
                    </fieldset>
                </g:form>
            </ul>
            <div class="pagination">
                <g:paginate total="${permissionInstanceTotal}" id="${groupRoleId}"/>
            </div>
		</div>
        <script>
        $(function() {
            $( "#permission-search" ).autocomplete({
                    source: "/YOUniversity/group/get-all-users"
                });
            });
        </script>
	</body>
</html>
