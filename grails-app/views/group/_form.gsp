<%@ page import="com.youniversity.core.user.Group" %>



<div class="fieldcontain ${hasErrors(bean: groupInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="group.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${groupInstance?.name}"/>
</div>
<div class="fieldcontain ${hasErrors(bean: groupInstance, field: 'description', 'error')} required">
    <label for="description">
        <g:message code="group.description.label" default="Description" />
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="description" required="" value="${groupInstance?.description}"/>
</div>


</div>

