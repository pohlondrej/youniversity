
<%@ page import="com.youniversity.core.user.Group" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${groupInstance.name}" />
		<title><g:message code="${groupInstance.name}" /></title>
	</head>
	<body>


		<div id="show-group" class="content scaffold-show" role="main">
            <div class="row"  style="background-color: red">
			    <h1><g:message code="${groupInstance.name}" />
                    <div class="studentProfilePictureWrapper" style="padding-right:10px">
                        <img src="${groupInstance?.picturePath == null ? resource(dir: '/images/default', file: 'defaultGroupProfilePicture.jpg') : createLink(controller: 'Group', action: 'getPictureFromDisk', id: groupInstance?.id)}"/>
                    </div>
                </h1>

            </div>
            <div class="row"  style="background-color: deeppink">

                <div class="small-8 large-4 columns" style="background-color: orange">
                    <g:each in="${groupInstance.groupRoles}" var="groupRole">
                        <div class="row"><h2>${groupRole.role.authority}</h2></div>
                        <g:each in="${users[groupRole.id]}" var="member">
                            <div class="row">${member.username}</div>
                        </g:each>
                    </g:each>
                </div>


                <div class="small-4 large-8 columns" style="background-color: blue"><h2>Description</h2>${groupInstance.getDescription()}



                </div>



               </div>
		</div>

	</body>
</html>
