<%@ page import="com.youniversity.core.user.Permission" %>



<div class="fieldcontain ${hasErrors(bean: permissionInstance, field: 'url', 'error')} required">
	<label for="url">
		<g:message code="permission.url.label" default="Url" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="url" required="" value="${permissionInstance?.url}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: permissionInstance, field: 'configAttribute', 'error')} required">
	<label for="configAttribute">
		<g:message code="permission.configAttribute.label" default="Config Attribute" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="configAttribute" required="" value="${permissionInstance?.configAttribute}"/>
</div>


<div class="fieldcontain ${hasErrors(bean: permissionInstance, field: 'application', 'error')} required">
	<label for="application">
		<g:message code="permission.application.label" default="Application" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="application" name="application.id" from="${com.youniversity.core.app.Application.list()}" optionKey="id" required="" value="${permissionInstance?.application?.id}" class="many-to-one"/>
</div>

