<%--
  Created by IntelliJ IDEA.
  User: richard
  Date: 12.8.13
  Time: 16:27
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.youniversity.core.MenuTagLib" %>

<div class="large-2 columns" style="position: fixed">
    <div class="docs section-container accordion" data-section="accordion">
        <section class="section">
                <g:set var="tmp" value="${grailsApplication?.getControllerClasses()?.find{new grails.web.HyphenatedUrlConverter().toUrlElement(it?.logicalPropertyName)==params?.controller}?.packageName}"/>
                <g:if test="${tmp?.startsWith("com.youniversity.apps")==true}">
                    <g:renderIfExists template="/${params.controller}/sidebar" plugin="${tmp.minus('com.youniversity.apps.').replaceAll(/\..*/,"")}" />
                </g:if>
                <g:else>
                    <g:renderIfExists template="/${params.controller}/sidebar"/>
                </g:else>
        </section>
    </div>
</div>
