<%--
  Created by IntelliJ IDEA.
  User: richard
  Date: 20.8.13
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><g:layoutTitle default="Grails"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
    <link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
    <link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
    <link rel="stylesheet" href="${resource(dir: 'foundation/css', file: 'foundation.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'foundation/css', file: 'normalize.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'default.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'component.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'sidepanel.css')}" type="text/css">
    <script src="/js/modernizr.custom.js"></script>
    <script src="/js/classie.js"></script>
    <script src="/js/application.js"></script>
    <script src="/js/vendor/custom.modernizr.js"></script>
    <script src="/js/sidepanel.js"></script>
    <s2ui:resources module='register' />
    <g:layoutHead/>
    <r:layoutResources />
</head>
<body>


<g:javascript src="classie.js" />
<g:javascript src="modernizr.custom.js" />
<s2ui:layoutResources module='register' />

<s2ui:showFlash/>
<div style="margin-top: 4em">
    <div class="large-12 columns" >
        <g:layoutBody/>
    </div>
</div>
<div class="footer" role="contentinfo"></div>
<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
</body>
</html>
