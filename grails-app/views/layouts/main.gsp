<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><g:layoutTitle default="Grails"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
    <link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
    <link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
    <link rel="stylesheet" href="${resource(dir: 'foundation/css', file: 'foundation.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'foundation/css', file: 'normalize.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'default.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'component.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'sidepanel.css')}" type="text/css">
    <script src="${resource(dir: 'js', file: 'modernizr.custom.js')}"></script>
    <script src="${resource(dir: 'js', file: 'classie.js')}"></script>
    <script src="${resource(dir: 'js', file: 'application.js')}"></script>
    <script src="${resource(dir: 'foundation/js/vendor', file: 'custom.modernizr.js')}"></script>
    <s2ui:resources module='register' />
    <g:layoutHead/>
    <r:layoutResources />
    <script src="${resource(dir: 'foundation/js', file: 'foundation.min.js')}"></script>
</head>
<body>

<div class="fixed">
    <nav class="top-bar">

        <ul class="title-area">
            <!-- Title Area -->
            <li>
                <button id="showLeft" style="background-color:#111111; border:none">:-)</button>
            </li>
            <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
        </ul>
        <ul class="title-area">
            <!-- Title Area -->
            <li class="name">
                <h1><a href="/YOUniversity">YOUniversity </a></h1>
            </li>
            <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
        </ul>

        <section class="top-bar-section">
            <!-- Left Nav Section -->
            <ul class="left">
                <li class="divider"></li>
                <li class="active"><g:link controller="group" action="list">Groups</g:link></li>
                <li class="divider"></li>
                <li><g:link controller="user" action="show" id="1">Profile</g:link></li>
                <li class="divider"></li>
                <li class="has-dropdown"><a href="#">Main Item 3</a>

                    <ul class="dropdown">
                        <li><a href="#">Dropdown Level 1b</a></li>
                        <li><a href="#">Dropdown Level 1c</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Dropdown Level 1d</a></li>
                        <li><a href="#">Dropdown Level 1e</a></li>
                        <li><a href="#">Dropdown Level 1f</a></li>
                        <li class="divider"></li>
                        <li><a href="#">See all &rarr;</a></li>
                    </ul>
                </li>
                <li class="divider"></li>
            </ul>
        </section>
    </nav>
</div>

<s2ui:layoutResources module='register' />

<s2ui:showFlash/>
<div style="margin-top: 1em">
    <g:render template="/layouts/menu"></g:render>
    <g:render template="/layouts/sideMenu"></g:render>
    <div class="large-2 columns" >
        <p class="title"></p>
    </div>
    <div class="large-8 columns" >
        <g:layoutBody/>
    </div>
    <div class="large-2 columns" >
    </div>
</div>
<div class="footer" role="contentinfo"></div>
<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
<script>
    $(document).foundation();
</script>
</body>
</html>
