<%@ page import="com.youniversity.core.user.User" %>



<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="user.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" required="" value="${userInstance?.username}"/>
</div>


<div class="studentProfilePictureWrapper">
    <img src="${userInstance?.picturePath == null ? resource(dir: '/images/default', file: 'defaultProfilePicture.jpg') : createLink(controller: 'User', action: 'getPictureFromDisk', id: userInstance?.id)}"/>
</div>
<br>

<div class="form-field">
    <label for="myFile">
        <g:message code="user.profile.profilePicture.label"/>
    </label>

    <div class="empty-input">
        <input type="file" name="profilePicture" id="profilePicture" />

    </div>

</div>

