
<%@ page import="com.youniversity.core.user.User;com.youniversity.core.user.UserRole;com.youniversity.core.user.GroupRole " %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
	    <h1>${userInstance?.username} <g:link class="edit" action="edit" id="1"><g:message code="default.button.edit.label" default="Edit" /></g:link></h1>

    <div class="studentProfilePictureWrapper">

        <img src="${userInstance?.picturePath == null ? resource(dir: '/images/default', file: 'defaultProfilePicture.jpg') : createLink(controller: 'User', action: 'getPictureFromDisk', id: userInstance?.id)}"/>
    </div>
    <br>

        <div class="large-3 columns" >
            <h3>Favourite applications</h3>
            <ul>
            <g:each in="${userInstance?.favouriteApps.findAll {!it.widget}}" var="fav">
                <li>${fav.application.displayedName}</li>
            </g:each>
            </ul>
            <br />
            <h3>Groups</h3>
            <ul>
                <g:each in="${UserRole.findAllByUser(User.get(sec.loggedInUserInfo(field:"id"))).groupRole.group.unique()}" var="group">
                    <li>${group.name} (
                    <g:each in="${UserRole.findAllByUserAndGroupRoleInList(User.get(sec.loggedInUserInfo(field:"id")),GroupRole.findAllByGroup(group)).groupRole.role.authority}" var="role">
                        ${role}
                    </g:each>

                    )</li>
                </g:each>
            </ul>
        </div>
        <div class="large-9 columns" style="color: #003C78">

        </div>
	</body>
</html>
