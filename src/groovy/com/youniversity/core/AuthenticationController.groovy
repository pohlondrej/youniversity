package com.youniversity.core
import com.youniversity.core.user.UserRole

abstract class AuthenticationController {
    def authenticationService
    def springSecurityService

    def beforeInterceptor = {
        def converter =  new grails.web.HyphenatedUrlConverter()
        boolean authorized=false
        UserRole.findAllByUser(springSecurityService.getCurrentUser()).groupRole.each {
            def method = permissions?.getAt(converter.toUrlElement(actionUri))?.getAt("authMethod")?.getAt(it.authority)
            if(grailsApplication?.getArtefact("Service","com.youniversity.core.AuthenticationService")?.metaClass?.methods?.find{it.name==method})  {
                if(authenticationService?."${method}"()) {
                    authorized = true
                }
            }   else    {
                authorized =  true
            }
        }
        if(!authorized)
            redirect controller: "login", action: "denied"

        //TODO - add params

    }
}
