
<%@ page import="com.youniversity.apps.chat.Message" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="list" action="index">Message list</g:link></li>
				<li><g:link class="create" action="create">New message</g:link></li>
			</ul>
		</div>
		<div id="show-message" class="content scaffold-show" role="main">
			<h1>Show message</h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list message">
			
				<g:if test="${message?.lastEdited}">
				<li class="fieldcontain">
					<span id="lastEdited-label" class="property-label"><g:message default="Last Edited" /></span>
					
						<span class="property-value" aria-labelledby="lastEdited-label"><g:formatDate date="${message?.lastEdited}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${message?.date}">
				<li class="fieldcontain">
					<span id="date-label" class="property-label"><g:message code="message.date.label" default="Date" /></span>
					
						<span class="property-value" aria-labelledby="date-label"><g:formatDate date="${message?.date}" /></span>
					
				</li>
				</g:if>

				<g:if test="${message?.text}">
				<li class="fieldcontain">
					<span id="text-label" class="property-label"><g:message code="message.text.label" default="Text" /></span>
					
						<span class="property-value" aria-labelledby="text-label"><g:fieldValue bean="${message}" field="text"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:message, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${message}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="Delete" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
