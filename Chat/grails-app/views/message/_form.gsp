<%@ page import="com.youniversity.apps.chat.Message" %>

<div class="fieldcontain ${hasErrors(bean: message, field: 'text', 'error')} ">
    <label for="text">
        <g:message code="message.text.label" default="Text" />

    </label>
    <g:textField name="text" value="${message?.text}"/>
</div>
<div class="fieldcontain ${hasErrors(bean: message, field: 'editable', 'error')} ">
	<label for="editable">
		<g:message code="message.editable.label" default="Editable" />
		
	</label>
	<g:checkBox name="editable" value="${message?.editable}" />
</div>