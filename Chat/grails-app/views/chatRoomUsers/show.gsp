
<%@ page import="com.youniversity.apps.chat.ChatRoomUsers" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'chatRoomUsers.label', default: 'ChatRoomUsers')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-chatRoomUsers" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-chatRoomUsers" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list chatRoomUsers">
			
				<g:if test="${chatRoomUsers?.chatRoom}">
				<li class="fieldcontain">
					<span id="chatRoom-label" class="property-label"><g:message code="chatRoomUsers.chatRoom.label" default="Chat Room" /></span>
					
						<span class="property-value" aria-labelledby="chatRoom-label"><g:link controller="chatRoom" action="show" id="${chatRoomUsers?.chatRoom?.id}">${chatRoomUsers?.chatRoom?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${chatRoomUsers?.user}">
				<li class="fieldcontain">
					<span id="user-label" class="property-label"><g:message code="chatRoomUsers.user.label" default="User" /></span>
					
						<span class="property-value" aria-labelledby="user-label"><g:fieldValue bean="${chatRoomUsers}" field="user"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:chatRoomUsers, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${chatRoomUsers}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
