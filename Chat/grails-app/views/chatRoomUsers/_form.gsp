<%@ page import="com.youniversity.apps.chat.ChatRoomUsers" %>



<div class="fieldcontain ${hasErrors(bean: chatRoomUsers, field: 'chatRoom', 'error')} required">
	<label for="chatRoom">
		<g:message code="chatRoomUsers.chatRoom.label" default="Chat Room" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="chatRoom" name="chatRoom.id" from="${com.youniversity.apps.chat.ChatRoom.list()}" optionKey="id" required="" value="${chatRoomUsers?.chatRoom?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: chatRoomUsers, field: 'user', 'error')} required">
	<label for="user">
		<g:message code="chatRoomUsers.user.label" default="User" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="user" type="number" value="${chatRoomUsers.user}" required=""/>
</div>

