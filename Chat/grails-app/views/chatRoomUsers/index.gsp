
<%@ page import="com.youniversity.apps.chat.ChatRoomUsers" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'chatRoomUsers.label', default: 'ChatRoomUsers')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-chatRoomUsers" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-chatRoomUsers" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<th><g:message code="chatRoomUsers.chatRoom.label" default="Chat Room" /></th>
					
						<g:sortableColumn property="user" title="${message(code: 'chatRoomUsers.user.label', default: 'User')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${chatRoomUsersList}" status="i" var="chatRoomUsers">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${chatRoomUsers.id}">${fieldValue(bean: chatRoomUsers, field: "chatRoom")}</g:link></td>
					
						<td>${fieldValue(bean: chatRoomUsers, field: "user")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${chatRoomUsersCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
