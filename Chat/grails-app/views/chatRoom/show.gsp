
<%@ page import="com.youniversity.apps.chat.ChatRoom" %>
<%@ page import="com.youniversity.apps.chat.Message" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'chatRoom.label', default: 'ChatRoom')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="nav" role="navigation">
			<ul>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-chatRoom" class="content scaffold-show" role="main">
			<h1><g:fieldValue bean="${chatRoom}" field="name"/></h1>

            <g:each in="${chatRoom.messages}" status="i" var="message">
                <div class="panel columns" style="border-radius: 0.3em">
                    <div class="large-11 push-1 columns" style="min-height: 10em; hyphens: auto; word-wrap: break-word">
                        ${fieldValue(bean: message, field: "text")}
                    </div>
                    <div class="large-1 pull-11 columns" style="min-height: 10em; background-color: white; border: 1px solid #000000; border-radius: 0.3em">
                        Username
                    </div>

                </div>
            </g:each>


            <g:form url="[resource:chatRoom, action:'addResponse']">
            <div class="fieldcontain ${hasErrors(bean: Message, field: 'text', 'error')} ">
                <g:textField name="name"/>
                <g:actionSubmit class="create" action="addResponse" value="Send" />

            </div>
            </g:form>

		</div>
	</body>
</html>
