<%@ page import="com.youniversity.apps.chat.ChatRoom" %>

<div class="fieldcontain ${hasErrors(bean: chatRoom, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="chatRoom.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${chatRoom?.name}"/>
</div>

