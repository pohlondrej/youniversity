<%@ page import="com.youniversity.apps.chat.ChatRoom" %>
<h3>Threads</h3>
<g:each var="c" in="${ChatRoom.findAllByParentIsNull()}">
    <p class="title"><g:link controller="ChatRoom" action="show" id="${c.id}">${c.name}</g:link></p>
</g:each>