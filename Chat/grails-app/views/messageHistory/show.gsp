
<%@ page import="com.youniversity.apps.chat.MessageHistory" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'messageHistory.label', default: 'MessageHistory')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-messageHistory" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-messageHistory" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list messageHistory">
			
				<g:if test="${messageHistory?.text}">
				<li class="fieldcontain">
					<span id="text-label" class="property-label"><g:message code="messageHistory.text.label" default="Text" /></span>
					
						<span class="property-value" aria-labelledby="text-label"><g:fieldValue bean="${messageHistory}" field="text"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${messageHistory?.lastEdited}">
				<li class="fieldcontain">
					<span id="lastEdited-label" class="property-label"><g:message code="messageHistory.lastEdited.label" default="Last Edited" /></span>
					
						<span class="property-value" aria-labelledby="lastEdited-label"><g:formatDate date="${messageHistory?.lastEdited}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${messageHistory?.message}">
				<li class="fieldcontain">
					<span id="message-label" class="property-label"><g:message code="messageHistory.message.label" default="Message" /></span>
					
						<span class="property-value" aria-labelledby="message-label"><g:link controller="message" action="show" id="${messageHistory?.message?.id}">${messageHistory?.message?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${messageHistory?.user}">
				<li class="fieldcontain">
					<span id="user-label" class="property-label"><g:message code="messageHistory.user.label" default="User" /></span>
					
						<span class="property-value" aria-labelledby="user-label"><g:fieldValue bean="${messageHistory}" field="user"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:messageHistory, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${messageHistory}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
