<%@ page import="com.youniversity.apps.chat.MessageHistory" %>



<div class="fieldcontain ${hasErrors(bean: messageHistory, field: 'text', 'error')} ">
	<label for="text">
		<g:message code="messageHistory.text.label" default="Text" />
		
	</label>
	<g:textArea name="text" cols="40" rows="5" maxlength="20000" value="${messageHistory?.text}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: messageHistory, field: 'lastEdited', 'error')} ">
	<label for="lastEdited">
		<g:message code="messageHistory.lastEdited.label" default="Last Edited" />
		
	</label>
	<g:datePicker name="lastEdited" precision="day"  value="${messageHistory?.lastEdited}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: messageHistory, field: 'message', 'error')} required">
	<label for="message">
		<g:message code="messageHistory.message.label" default="Message" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="message" name="message.id" from="${com.youniversity.apps.chat.Message.list()}" optionKey="id" required="" value="${messageHistory?.message?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: messageHistory, field: 'user', 'error')} required">
	<label for="user">
		<g:message code="messageHistory.user.label" default="User" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="user" type="number" value="${messageHistory.user}" required=""/>
</div>

