
<%@ page import="com.youniversity.apps.chat.MessageHistory" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'messageHistory.label', default: 'MessageHistory')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-messageHistory" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-messageHistory" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="text" title="${message(code: 'messageHistory.text.label', default: 'Text')}" />
					
						<g:sortableColumn property="lastEdited" title="${message(code: 'messageHistory.lastEdited.label', default: 'Last Edited')}" />
					
						<th><g:message code="messageHistory.message.label" default="Message" /></th>
					
						<g:sortableColumn property="user" title="${message(code: 'messageHistory.user.label', default: 'User')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${messageHistoryList}" status="i" var="messageHistory">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${messageHistory.id}">${fieldValue(bean: messageHistory, field: "text")}</g:link></td>
					
						<td><g:formatDate date="${messageHistory.lastEdited}" /></td>
					
						<td>${fieldValue(bean: messageHistory, field: "message")}</td>
					
						<td>${fieldValue(bean: messageHistory, field: "user")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${messageHistoryCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
