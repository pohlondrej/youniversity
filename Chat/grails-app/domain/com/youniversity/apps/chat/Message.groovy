package com.youniversity.apps.chat


class Message {

    String text
    Date date = new Date()
    Date lastEdited = new Date()
    Boolean editable = false
    ChatRoom chatRoom

    static constraints = {
        lastEdited nullable: true
        date nullable: true
        text(maxSize: 20000)
    }

    def afterUpdate() {
        lastEdited = new Date()
    }
}
