package com.youniversity.apps.chat

class ChatRoom {

    String name
    ChatRoom parent

    static hasMany = [messages: Message]


    static constraints = {
        messages nullable: true
        parent nullable: true
    }
}
