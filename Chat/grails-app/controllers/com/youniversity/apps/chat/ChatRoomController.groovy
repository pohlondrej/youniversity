package com.youniversity.apps.chat



import grails.transaction.*
import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class ChatRoomController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [ chatRoomList:ChatRoom.list(params), chatRoomCount: ChatRoom.count()]
    }

    def show(ChatRoom chatRoom) {
        [chatRoom:chatRoom,messageList:Message]
    }

    def create() {
        respond new ChatRoom(params)
    }

    @Transactional
    def save(ChatRoom chatRoom) {
        if (chatRoom.hasErrors()) {
            render(view: "create", model: [chatRoom: chatRoom])
        } else {
            chatRoom.save flush: true
            request.withFormat {
                form {
                    redirect(controller: "ChatRoom", action: "show", id: chatRoom.id)
                }
                '*' { respond chatRoom, [status: CREATED] }
            }
        }
    }

    def edit(ChatRoom chatRoom) {
        [chatRoom:chatRoom]
    }

    @Transactional
    def update(ChatRoom chatRoom) {
        if (chatRoom == null) {
            render status: 404
        } else if (chatRoom.hasErrors()) {
            respond chatRoom.errors, view: 'edit'
        } else {
            chatRoom.save flush: true
            request.withFormat {
                form {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'ChatRoom.label', default: 'ChatRoom'), chatRoom.id])
                    redirect chatRoom
                }
                '*' { respond chatRoom, [status: OK] }
            }
        }
    }

    @Transactional
    def delete(ChatRoom chatRoom) {
        if (chatRoom) {
            chatRoom.delete flush: true
            request.withFormat {
                form {
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'ChatRoom.label', default: 'ChatRoom'), chatRoom.id])
                    redirect action: "index", method: "GET"
                }
                '*' { render status: NO_CONTENT }
            }
        } else {
            render status: NOT_FOUND
        }
    }

    def addResponse(Long id) {
        Message message = new Message()
        message.text = params.name
        message.chatRoom=ChatRoom.findById(id)
        if(message.hasErrors()) {
            render(view: "show")
        }
        else {
            message.save flush:true, failOnError: true
            request.withFormat {
                form {
                    redirect action:"show", id:id
                }
                '*' { respond message, [status: CREATED] }
            }
        }
    }
}

