package com.youniversity.apps.chat



import grails.transaction.*
import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class MessageHistoryController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond MessageHistory.list(params), model: [messageHistoryCount: MessageHistory.count()]
    }

    def show(MessageHistory messageHistory) {
        respond messageHistory
    }

    def create() {
        respond new MessageHistory(params)
    }

    @Transactional
    def save(MessageHistory messageHistory) {
        if (messageHistory.hasErrors()) {
            respond messageHistory.errors, view: 'create'
        } else {
            messageHistory.save flush: true
            request.withFormat {
                form {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'messageHistory.label', default: 'MessageHistory'), messageHistory.id])
                    redirect messageHistory
                }
                '*' { respond messageHistory, [status: CREATED] }
            }
        }
    }

    def edit(MessageHistory messageHistory) {
        respond messageHistory
    }

    @Transactional
    def update(MessageHistory messageHistory) {
        if (messageHistory == null) {
            render status: 404
        } else if (messageHistory.hasErrors()) {
            respond messageHistory.errors, view: 'edit'
        } else {
            messageHistory.save flush: true
            request.withFormat {
                form {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'MessageHistory.label', default: 'MessageHistory'), messageHistory.id])
                    redirect messageHistory
                }
                '*' { respond messageHistory, [status: OK] }
            }
        }
    }

    @Transactional
    def delete(MessageHistory messageHistory) {
        if (messageHistory) {
            messageHistory.delete flush: true
            request.withFormat {
                form {
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'MessageHistory.label', default: 'MessageHistory'), messageHistory.id])
                    redirect action: "index", method: "GET"
                }
                '*' { render status: NO_CONTENT }
            }
        } else {
            render status: NOT_FOUND
        }
    }
}

