package com.youniversity.apps.chat





import grails.transaction.*
import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class MessageController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [ messageList:Message.list(params), messageCount: Message.count()]
    }

    def show(Message message) {
        [message:message]
    }

    def create() {        
        respond new Message(params)
    }

    @Transactional
    def save(Message message) {
        //message.date = message.createdAt()
        //message.lastEdited = message.updatedAt()
        if(message.hasErrors()) {
            render(view: "create", model: [message: message])
        }
        else {

           // message.date = message.createdAt()
          //  message.lastEdited = message.updatedAt()
            message.save flush:true
            request.withFormat {
                form {
                    redirect message
                }
                '*' { respond message, [status: CREATED] }
            }
        }
    }

    def edit(Message message) {
        [message:message]
    }

    @Transactional
    def update(Message message) {
        if(message == null) {
            render status:404
        }
        else if(message.hasErrors()) {
            render(view: "edit", model: [message: message])
        }
        else {
            message.save flush:true
            request.withFormat {
                form {
                    redirect message 
                }
                '*'{ respond message, [status: OK] }
            }
        }        
    }

    @Transactional
    def delete(Message message) {
        if(message) {
            message.delete flush:true
            request.withFormat {
                form {
                    redirect action:"index", method:"GET" 
                }
                '*'{ render status: NO_CONTENT }
            }                
        }
        else {
            render status: NOT_FOUND
        }
    }
}

