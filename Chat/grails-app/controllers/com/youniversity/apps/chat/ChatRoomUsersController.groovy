package com.youniversity.apps.chat



import grails.transaction.*
import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class ChatRoomUsersController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ChatRoomUsers.list(params), model: [chatRoomUsersCount: ChatRoomUsers.count()]
    }

    def show(ChatRoomUsers chatRoomUsers) {
        respond chatRoomUsers
    }

    def create() {
        respond new ChatRoomUsers(params)
    }

    @Transactional
    def save(ChatRoomUsers chatRoomUsers) {
        if (chatRoomUsers.hasErrors()) {
            respond chatRoomUsers.errors, view: 'create'
        } else {
            chatRoomUsers.save flush: true
            request.withFormat {
                form {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'chatRoomUsers.label', default: 'ChatRoomUsers'), chatRoomUsers.id])
                    redirect chatRoomUsers
                }
                '*' { respond chatRoomUsers, [status: CREATED] }
            }
        }
    }

    def edit(ChatRoomUsers chatRoomUsers) {
        respond chatRoomUsers
    }

    @Transactional
    def update(ChatRoomUsers chatRoomUsers) {
        if (chatRoomUsers == null) {
            render status: 404
        } else if (chatRoomUsers.hasErrors()) {
            respond chatRoomUsers.errors, view: 'edit'
        } else {
            chatRoomUsers.save flush: true
            request.withFormat {
                form {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'ChatRoomUsers.label', default: 'ChatRoomUsers'), chatRoomUsers.id])
                    redirect chatRoomUsers
                }
                '*' { respond chatRoomUsers, [status: OK] }
            }
        }
    }

    @Transactional
    def delete(ChatRoomUsers chatRoomUsers) {
        if (chatRoomUsers) {
            chatRoomUsers.delete flush: true
            request.withFormat {
                form {
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'ChatRoomUsers.label', default: 'ChatRoomUsers'), chatRoomUsers.id])
                    redirect action: "index", method: "GET"
                }
                '*' { render status: NO_CONTENT }
            }
        } else {
            render status: NOT_FOUND
        }
    }
}

