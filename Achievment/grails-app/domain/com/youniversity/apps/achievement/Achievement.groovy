package com.youniversity.apps.achievement

class Achievement {

    String name
    //User user
    String description
    String ico_path
    //Application app
    //Grouprole grouprole
    int difficulty
    int experience

    static hasMany = [achievementCriterias:AchievementCriteria]

    static constraints =  {
        name nullable:true
        description nullable:true
        ico_path nullable:true
    }
}
