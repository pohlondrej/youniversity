package com.youniversity.apps.achievement

class AchievementLevel {
    String name
    String description
    Integer experience
    String ico

    static constraints = {
        name nullable:true
        description nullable:true
    }
}
