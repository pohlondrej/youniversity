package com.youniversity.apps.achievement

class AchievementCategory {
    String name
    AchievementCategory parent

    static hasMany = [Achievement:Achievement]

    static constraints = {
        name nullable:true
    }
}
